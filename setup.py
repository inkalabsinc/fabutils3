import os
from setuptools import setup, find_packages

def read(fname):
    return open(os.path.join(os.path.dirname(__file__), fname)).read()

setup(
    name="fabutils3",
    version="0.1.0.0",
    author="InkaLabs",
    author_email="inkalabs@inka-labs.com",
    description=("Super usefull tools for Fabric3"),
    license="Freeware",
    keywords="fabric",
    url="https://bitbucket.org/inkalabsinc/fabutils/",
    packages=['fabutils'],
    long_description=read('README.md'),
    install_requires=['fabric3'],
    classifiers=[
        "Development Status :: 4 - Beta",
        "Natural Language :: English",
        "Programming Language :: Python :: 3.4",
        "Topic :: Utilities",
        "License :: Freeware",
    ],
)
