**PLEASE DO NOT USE THIS REPO, INSTEAD USE https://bitbucket.org/inkalabsinc/fabutils **

Usefull Fabric tools, 
in order to use it, just install the package, 
then create a fabfile like this


import fabutils

fabutils.autodiscover_environments()


class Deploy(fabutils.GunicornMixin,
             fabutils.VirtualenvMixin, fabutils.Deployment):
    """
    Base deployment class, do not use it directly in the commands
    """
    database_handler = fabutils.MySqlDatabaseBackup

fabutils.register_class(Deploy)
fabutils.register_class(fabutils.LocalDatabaseOperations)
fabutils.register_class(fabutils.RemoteDatabaseOperations)




where fabutils.autodiscover_environments() will see for your
environments defined in your settings file like this


ENVIRONMENTS = {
    'prod': {
        'hosts': ['XXXX@192.168.XX.XX'],
        'password': 'XXXX',
        'core_dir': '/home/XXX/XXXX/XXXX/',
        'gunicorn_pid': "/tmp/XX.pid",
        'gunicorn_server_socket': "127.0.0.1:9000",
        'gunicorn_user': "XXX",
        'is_production': True,
        'branch': 'prod',
    },
    'dev': {
        'hosts': ['XX@XXXXXX'],
        'password': 'XXXXX',
        'core_dir': '/home/XXXX/XXXXX/XXXX/XXX/',
        'gunicorn_pid': "/tmp/XXXX.pid",
        'gunicorn_server_socket': "127.0.0.1:9091",
        'gunicorn_user': "XXX",
        'is_production': False,
        'branch': 'staging',
    },
    'local': {
        'hosts': ['127.0.0.1'],
        'core_dir': '/Users/XXXX/XXXXX/XXX',
        'is_production': False,
    },

}





Of course you can overwrite or subclass the Deploy class