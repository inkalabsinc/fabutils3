import os

from fabric.api import prompt, cd, sudo, run, env, get, hide, local
from fabric.colors import green, red, yellow
from .databases import RemoteDatabaseOperations


class Deployment(object):
    settings = None
    requirements_file = 'requirements.txt'

    def run_raw_command(self, cmd):
        return run(cmd)

    def __init__(self):
        self.env = env
        if 'is_production' in self.env and self.env.is_production:
            red('You are about to run comamnds in the production server.'
                'Please be careful!')

    def fab_install_requirements(self):
        """
        Installs python requirements from requirements.txt
        """
        print(green("Installing python requirements"))
        self.fab_run_env('pip install -r %s -U' % self.requirements_file)

    def fab_migrate(self):
        """
        Runs database migrations
        """
        print(green("Running database migrations"))
        self.fab_run_env('python manage.py migrate')

    def fab_clean_pyc_files(self):
        """
        Removes all *.pyc files
        """
        print(green("Deleting .pyc files"))
        self.run_raw_command(
            'cd %s && find . -iname "*.pyc" -delete' % self.env.core_dir)

    def fab_collectstatic(self):
        """
        Collects app static files
        """
        print(green("Collecting static files from apps"))
        self.fab_run_env('python manage.py collectstatic --noinput')

    def fab_update_repo(self):
        """
        Updates the git repository with the branch attached to this server
        (see environmets.py)
        """
        print(green("Updating repo"))
        self.run_raw_command('git fetch --all')
        self.run_raw_command('git checkout %s' % env.branch)
        self.run_raw_command('git pull origin %s' % env.branch)

    def fab_create_local_static_bundle(self):
        """
        Bundle static files to the local machine
        """
        print(green("Packing static files"))
        run("rm static.tar.gz")
        run("tar -zcvf static.tar.gz {0}".format(self.settings.STATIC_ROOT))
        print(green("Static dump created"))

    def fab_download_static(self):
        """
        Download static files to the local machine
        """
        self.fab_run_env(
            "fab create_local_static_bundle -f %sfabfile.py" % env.core_dir)
        print(green("Packing static files"))
        run("tar -zcvf static.tar.gz {0}".format(env.core_dir))
        print(green("You're gonna download: "))
        run("du -hs static.tar.gz")
        files = get("static.tar.gz")
        for f in files:
            print(green("Download file %s, now you can copy it" % f))
            run("rm static.tar.gz")

    def fab_restart_instance(self):
        raise NotImplementedError("Please Implement this method")

    def fab_deploy(self):
        """
        Deploys the system to a specified server (check envrionments.py)
        """
        if self.env.is_production and not prompt(
                red("Do you REALLY wanna deploy to PRODUCTION SERVER? "
                    "Type 'yes' if you know what are you doing."
                    )).lower() == 'yes':
            return

        with cd(self.env.core_dir):
            db_handler = RemoteDatabaseOperations()
            db_handler.fab_make_remote_backup()
            self.fab_update_repo()
            self.fab_install_requirements()
            self.fab_migrate()
            self.fab_clean_pyc_files()
            self.fab_collectstatic()
            self.fab_restart_instance()


class GunicornMixin(object):
    def fab_restart_instance(self):
        """
        Restarts Gunicorn
        """
        sudo('kill -QUIT `cat %s`' % self.env.gunicorn_pid)


class UwsgiMixin(object):
    def fab_restart_instance(self):
        """
        Restarts UWSGI
        """
        sudo('/etc/init.d/uwsgi restart')

    def fab_download_media(self):
        """
        Replicate the media directory from one of the environments in a local
        installation
        """
        if os.path.isfile("./fabfile.py"):
            with cd(self.env.core_dir):
                with hide('output'):
                    media_size = run("du -hs ../media")
                    cache_size = run("du -hs ../media/cache")
                    print(
                        green("Select the media files you want to download:"))
                    print(("     1). Media -- Size(%s)" % media_size) +
                          yellow("  This option will download all the media"
                                 " files."))
                    print(("     2). Cache -- Size(%s)" % cache_size) +
                          yellow("  This option will download just Cache "
                                 "folder, this option might be enough for "
                                 "most pages."))
                option = input("Option number: ")
                if int(option) == 1:
                    selected_option = "media"
                elif int(option) == 2:
                    selected_option = "media/cache"
                else:
                    print(red("The option number you entered is not valid."))
                    return
                print(green("Packing Media Files"))
                run("tar -zcvf /tmp/media.tar.gz ../%s" % selected_option)
                get("/tmp/media.tar.gz")
                run("rm /tmp/media.tar.gz")
            local("tar -xvf %s/media.tar.gz -C ../" % self.env.hosts[0])
            local("rm -rf %s" % self.env.hosts[0])
            print(green("Media Files has been downloaded successfully. "))
        else:
            print(red("Error: Please go to the 'fabfile.py' directory and try"
                      "again."))
            return
